/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

  gcc -Wall `pkg-config fuse --cflags --libs` fusexmp.c -o fusexmp
*/

#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <strings.h>
#include <sys/mman.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif
#include "fuse_ds.h"
#include <stdlib.h>


int i;
void *p;
int fd;
/* Initialize superblock and freelist and inode */
struct fs_superblock sb;
struct fs_inode inode;
struct block data;
uint32_t free_list_map[NUM_DATA_BLOCKS]; //= //(uint32_t *) malloc(NUM_DATA_BLOCKS * sizeof(int));
int bytes = 0;

static const char *hello_str = "Hello World!\n";
static const char *hello_path = "/hello";

static int hello_getattr(const char *path, struct stat *stbuf)
{
        int res = 0;

        memset(stbuf, 0, sizeof(struct stat));
        if (strcmp(path, "/") == 0) {
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
        } else if (strcmp(path, hello_path) == 0) {
                stbuf->st_mode = S_IFREG | 0444;
                stbuf->st_nlink = 1;
                stbuf->st_size = strlen(hello_str);
        } else
                res = -ENOENT;

        return res;
}
static int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi)
{
        (void) offset;
        (void) fi;

        if (strcmp(path, "/") != 0)
                return -ENOENT;

        filler(buf, ".", NULL, 0);
        filler(buf, "..", NULL, 0);
        filler(buf, hello_path + 1, NULL, 0);

        return 0;
}

static int hello_open(const char *path, struct fuse_file_info *fi)
{
        if (strcmp(path, hello_path) != 0)
                return -ENOENT;

        if ((fi->flags & 3) != O_RDONLY)
                return -EACCES;

        return 0;
}

static int hello_read(const char *path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi)
{
        size_t len;
        (void) fi;
        if(strcmp(path, hello_path) != 0)
                return -ENOENT;

        system("echo 'hello world log33' >> /log2.txt");
        len = strlen(hello_str);
        if (offset < len) {
                if (offset + size > len)
                        size = len - offset;
                memcpy(buf, hello_str + offset, size);
        } else
                size = 0;

        return size;
}


static struct fuse_operations hello_oper = {
        .getattr        = hello_getattr,
        .readdir        = hello_readdir,
	.open           = hello_open,
    	.read           = hello_read,
};

void mmap_file() {

	fd = open("test.img", O_RDWR);
        /* memory mapped */
        p = mmap(NULL,0,PROT_NONE,0,fd,0);
<<<<<<< HEAD
}

void init_superblock() {

        /* Zeroing the structures just to be on safer side */
        bzero(&sb, sizeof(struct fs_superblock));
        bzero(&inode, sizeof(struct fs_inode));

        /* Setting lseek to beginning of file */
        if(lseek(fd, 0 , SEEK_SET) == -1)
                perror("\n Seek error\n");

        /* Initialize superblock */
        sb.fs_size = FS_SIZE;
        sb.num_of_free_blks = NUM_DATA_BLOCKS;
        sb.head_data = 0;
        sb.num_of_free_inodes = MAX_INODES;
        sb.head_inode = 0;
        sb.superblock_mod = 0;
        /* write superblock to block 0 */
=======
	
	/* Initialize superblock and freelist and inode */
	struct fs_superblock sb;
	struct fs_inode inode;
	struct block data;
	uint32_t *free_list_map =  malloc(NUM_DATA_BLOCKS * sizeof(int));
	uint32_t *free_list_inodemap = malloc(MAX_INODES * sizeof(int));


	for (i = 0; i < MAX_INODES - 1; i++) {
		free_list_inodemap[i] = i + 1;
	}
	free_list_inodemap[MAX_INODES -1] = 0;

	for (i = 0; i < NUM_DATA_BLOCKS -1; i++) {
		free_list_map[i] = i + 1;
	}
	free_list_map[NUM_DATA_BLOCKS -1] = 0;

	/* Zeroing the structures just to be on safer side */
	bzero(&sb, sizeof(struct fs_superblock));
	bzero(&inode, sizeof(struct fs_inode));

	/* Setting lseek to beginning of file */
	if(lseek(fd, 0 , SEEK_SET) == -1)
		perror("\n Seek error\n");

	/* Initialize superblock */
	sb.fs_size = FS_SIZE;
	sb.num_of_free_blks = 7680; 
	sb.head_data = 0;
	sb.num_of_free_inodes = MAX_INODES;
	sb.head_inode = 0;
	sb.superblock_mod = 0;
	/* write superblock to block 0 */
>>>>>>> 622fdf1c2bb041d8fb2a603e7861a63e5283b7ce
       if(write(fd,&sb, sizeof(struct fs_superblock)) != sizeof(sb))
                printf("\n Error writing Superblock \n");
}

void init_inodeblock() {
      	lseek(fd, 0, SEEK_SET);
        lseek(fd, FS_BLOCK_SIZE, SEEK_SET);

        /* Write INODES to block 1 */
        for(i= 0; i < MAX_INODES; i++) {
                inode.inode_num = i + 1;
                inode.free_flag = 1;
                if(write(fd, &inode, sizeof(struct fs_inode)) != sizeof(inode))
                        printf("\n Error writing Inode \n");
                bytes += sizeof(inode);
        }
        printf("\n Bytes read %d\n", bytes);
}

void init_freelists()
{
	for (i = 0; i < NUM_DATA_BLOCKS ; i++) {
                free_list_map[i] = i + 1;
        }

	lseek(fd, 0, SEEK_SET);
        lseek(fd, 2 * FS_BLOCK_SIZE, SEEK_SET);
        
	/* Writing list of free data blocks to Block 2 */
       if(write(fd, free_list_map, sizeof(free_list_map)) != sizeof(free_list_map))
                printf("\n Error writing free list map\n");
}

void init_datablocks()
{
       lseek(fd, 0, SEEK_SET);
        lseek(fd, 10 * FS_BLOCK_SIZE, SEEK_SET);

        /* Writing all data blocks to raw DISK  AKA FaaFS ( File as a FileSystem */
        for(i=0; i < NUM_DATA_BLOCKS; i++) {
        //      sprintf(data.data,"hello%d", i+1);      
       		if(write(fd, &data, sizeof(struct block)) != sizeof(struct block))
			printf("\n Error writing data block %d\n", i);
        }
}

#if 0
void init_rootdirectory() {


}
#endif

void mkfs()
{
	mmap_file();
	init_superblock();
	init_inodeblock();
	init_freelists();
	init_datablocks();
//      init_rootdirectory();
}

int main(int argc, char*argv[]) {
	/* Make file system */
	mkfs();
		                                            
	return fuse_main(argc, argv, &hello_oper, NULL);
}
