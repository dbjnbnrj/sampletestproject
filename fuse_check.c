/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

  gcc -Wall `pkg-config fuse --cflags --libs` fusexmp.c -o fusexmp
*/

#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <strings.h>
#include <sys/mman.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif
#include "fuse_ds_latest.h"
#include <stdlib.h>


int i;
void *p;
int fd;


void chkfs()
{
	int bytes = 0;
        fd = open("test.img", O_RDWR);
	
	/* Read superblock freelist and inode */
	struct fs_superblock sb;
	struct fs_inode inode;
	struct block data;
	uint32_t free_list_map[NUM_DATA_BLOCKS]; //= //(uint32_t *) malloc(NUM_DATA_BLOCKS * sizeof(int));
	
	/* Zeroing the structures just to be on safer side */
	bzero(&sb, sizeof(struct fs_superblock));
	bzero(&inode, sizeof(struct fs_inode));
	bzero(&data, sizeof(struct block));
	/* Setting lseek to beginning of file */
	if(lseek(fd, 0 , SEEK_SET) == -1)
		perror("\n Seek error\n");

	/* check superblock */
	if(read(fd, &sb,sizeof(struct fs_superblock)) != sizeof(struct fs_superblock))
		printf("\nread error\n");
/*	
	sb.fs_size = FS_SIZE;
        sb.num_of_free_blks = 7674;
        sb.head_data = 0;
        sb.num_of_free_inodes = MAX_INODES;
        sb.head_inode = 0;
        sb.superblock_mod = 0;
*/
	printf("\n----- Maccheroni FS------\n");
	printf("\n ------SuperBlock--------");
	printf("\n Free Blocks %d", sb.num_of_free_blks);
	printf("\n FS size %d", sb.fs_size);
	printf("\n Free Inodes %d", sb.num_of_free_inodes); 
	

	lseek(fd, 0, SEEK_SET);
	lseek(fd, FS_BLOCK_SIZE, SEEK_SET);

	/* Read INODES from block 1 */
	printf("\n-------INODE Table---- SIZE %d", MAX_INODES);
	for(i=0; i < MAX_INODES; i++) {
       		if(read(fd, &inode, sizeof(struct fs_inode)) != sizeof(inode))
			printf("\n Error reading Inode \n");
		bytes += sizeof(inode);
		printf("\n Inode Num  %d Inode Alloc Status %d bytes read %d", inode.inode_num, inode.free_flag, bytes);
	}


	/* Writing list of free data blocks to Block 2 */
	printf("\n------ FREE LIST------");
       if(read(fd, free_list_map, sizeof(free_list_map)) != sizeof(free_list_map))
		printf("\n Error reading free list map\n");
	for(i = 0; i < NUM_DATA_BLOCKS;i++) {
		printf("\n Free Data Block %d %d", i, free_list_map[i]);

	}

	/* Note: Internal Fragmentation in Block 9 */	

	lseek(fd, 0, SEEK_SET);
	lseek(fd, 10 * FS_BLOCK_SIZE, SEEK_SET);

	/* Writing all data blocks to raw DISK  AKA FaaFS ( File as a FileSystem */
	printf("\n-----DATA BLOCKS------\n");
	bytes = 0;
	for(i=0; i < NUM_DATA_BLOCKS; i++) {
	
       		if(read(fd, &data, sizeof(struct block)) != sizeof(struct block))
			printf("\n Error writing data block %d\n", i);
		bytes += sizeof(struct block);
		printf("\n Bytes %d Data Block %d Data %s", bytes, i, data.data);
	
	}
}

int main(int argc, char*argv[]) {
	chkfs();
	return 0;	                                            
//	return fuse_main(argc, argv, &hello_oper, NULL);
}
